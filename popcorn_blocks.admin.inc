<?php


/**
 * Form builder for the add image block form.
 *
 * @see block_add_block_form_validate()
 * @see popcorn_blocks_add_block_form_submit()
 * @ingroup forms
 */
function popcorn_blocks_add_block_form() {
  module_load_include('inc', 'block', 'block.admin');
  $form = array();
  $form = block_admin_configure($form, $form_state, 'popcorn_blocks', NULL);
  $form['#validate'][] = 'block_add_block_form_validate';
  $form['#validate'][] = 'popcorn_blocks_configure_form_validate';
  return $form;
}

/**
 * Form submission handler for the add image block form.
 *
 * Saves the new custom image block.
 *
 * @see popcorn_blocks_add_block_form()
 * @see block_add_block_form_validate()
 */
function popcorn_blocks_add_block_form_submit($form, &$form_state) {
  $delta = db_insert('popcorn_blocks')
    ->fields(array(
      'body' => $form_state['values']['body']['value'],
      'info' => $form_state['values']['info'],
      'format' => $form_state['values']['body']['format'],
    ))
    ->execute();
  // Store block delta to allow other modules to work with new block.
  $form_state['values']['delta'] = $delta;

  $query = db_insert('block')->fields(array('visibility', 'pages', 'custom', 'title', 'module', 'theme', 'status', 'weight', 'delta', 'cache'));
  foreach (list_themes() as $key => $theme) {
    if ($theme->status) {
      $query->values(array(
        'visibility' => (int) $form_state['values']['visibility'],
        'pages' => trim($form_state['values']['pages']),
        'custom' => (int) $form_state['values']['custom'],
        'title' => $form_state['values']['title'],
        'module' => $form_state['values']['module'],
        'theme' => $theme->name,
        'status' => 0,
        'weight' => 0,
        'delta' => $delta,
        'cache' => DRUPAL_NO_CACHE,
      ));
    }
  }
  $query->execute();

  $query = db_insert('block_role')->fields(array('rid', 'module', 'delta'));
  foreach (array_filter($form_state['values']['roles']) as $rid) {
    $query->values(array(
      'rid' => $rid,
      'module' => $form_state['values']['module'],
      'delta' => $delta,
    ));
  }
  $query->execute();

  // Store regions per theme for this block
  foreach ($form_state['values']['regions'] as $theme => $region) {
    db_merge('block')
      ->key(array('theme' => $theme, 'delta' => $delta, 'module' => $form_state['values']['module']))
      ->fields(array(
        'region' => ($region == BLOCK_REGION_NONE ? '' : $region),
        'pages' => trim($form_state['values']['pages']),
        'status' => (int) ($region != BLOCK_REGION_NONE),
      ))
      ->execute();
  }

  popcorn_blocks_block_save($delta, $form_state['values']);

  drupal_set_message(t('The image block has been created.'));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/block';
}

/**
 * Form builder for the image block deletion form.
 *
 * @param $delta
 *   The unique ID of the block within the context of $module.
 *
 * @see popcorn_blocks_custom_block_delete_submit()
 */
function popcorn_blocks_custom_block_delete($form, &$form_state, $delta) {
  $block = block_load('popcorn_blocks', $delta);
  $custom_block = block_popcorn_blocks_get($block->delta);
  $form['info'] = array('#type' => 'hidden', '#value' => $custom_block['info'] ? $custom_block['info'] : $custom_block['title']);
  $form['bid'] = array('#type' => 'hidden', '#value' => $block->delta);

  return confirm_form($form, t('Are you sure you want to delete the image block %name?', array('%name' => $custom_block['info'])), 'admin/structure/block', '', t('Delete'), t('Cancel'));
}

/**
 * Form submission handler for the image block deletion form.
 *
 * @see popcorn_blocks_custom_block_delete()
 */
function popcorn_blocks_custom_block_delete_submit($form, &$form_state) {
  // If there is a file, delete it.
  $file = popcorn_blocks_get_file($form_state['values']['bid']);
  if (!empty($file->fid)) {
    file_usage_delete($file, 'popcorn_blocks', 'popcorn_blocks', $form_state['values']['bid']);
    file_delete($file);
  }
  
  db_delete('popcorn_blocks')
    ->condition('bid', $form_state['values']['bid'])
    ->execute();
  db_delete('block')
    ->condition('module', 'popcorn_blocks')
    ->condition('delta', $form_state['values']['bid'])
    ->execute();
  db_delete('block_role')
    ->condition('module', 'popcorn_blocks')
    ->condition('delta', $form_state['values']['bid'])
    ->execute();
  
  drupal_set_message(t('The image block %name has been removed.', array('%name' => $form_state['values']['info'])));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/block';
  return;
}
